﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Client.ViewModels.Shared;
using HtmlAgilityPack;

namespace Client.Common
{
    public class ClientResultHelper : ResultHelper
    {
        public override async Task<ResultViewModel> GetResult(string resultStr)
        {
            var task = new Task<ResultViewModel>(() =>
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(resultStr);

                var model = new ResultViewModel();

                try
                {
                    var mainTable = doc.DocumentNode.Element("html").Element("body").Element("table");

                    var header = mainTable.Element("tr").Element("td").Element("h4").InnerHtml;
                    var firstBrIndex = header.IndexOf("<br>", StringComparison.Ordinal);
                    var secondBrIndex = header.IndexOf("<br>", firstBrIndex + 1, StringComparison.Ordinal);
                    model.Title = header.Substring(0, firstBrIndex).Trim();
                    model.Date = header.Substring(firstBrIndex + 4, secondBrIndex - (firstBrIndex + 4)).Trim();

                    var stopsContainer = mainTable.Elements("tr").ElementAt(1).Element("td");

                    var dtToday = DateTime.Today;
                    StopScheduleViewModel currentStop = null;
                    foreach (var childNode in stopsContainer.ChildNodes)
                    {
                        if (childNode.Name == "h2")
                        {
                            currentStop = new StopScheduleViewModel { Name = System.Net.WebUtility.HtmlDecode(childNode.InnerText) };
                        }
                        else if (childNode.Name == "table" && currentStop != null)
                        {
                            // Parse table

                            foreach (var scheduleTr in childNode.Elements("tr"))
                            {
                                var dtNodes = scheduleTr.Elements("td").ToList();

                                for (var i = 0; i < dtNodes.Count; i++)
                                {
                                    var scheduleTd = dtNodes[i];
                                    if (scheduleTd.Attributes.Contains("class") &&
                                        scheduleTd.Attributes["class"].Value == "td_plan_h" && dtNodes.Count >= i + 2 && dtNodes[i + 1].Attributes.Contains("class") && dtNodes[i + 1].Attributes["class"].Value == "td_plan_m")
                                    {
                                        var hour = int.Parse(scheduleTd.InnerText);
                                        var scheduleTdMin = dtNodes[i + 1];
                                        var times =
                                            scheduleTdMin.ChildNodes.Where(
                                                c => c.Name == "div" && !string.IsNullOrWhiteSpace(c.InnerText))
                                                .Select(
                                                    c =>
                                                        new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, hour,
                                                            int.Parse(c.InnerText), 0));

                                        foreach (var time in times)
                                            currentStop.TimeList.Add(time);
                                    }
                                }
                            }
                            currentStop.Calculate();
                            model.Schedule.Add(currentStop);
                            currentStop = null;
                        }
                    }

                    return model;
                }
                catch (Exception exc)
                {
                    return null;
                }
            });

            task.Start();
            return await task;
        }
    }
}
