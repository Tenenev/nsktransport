﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.ViewModels.Shared
{
    public class RouteViewModel
    {
        public string Id { get; set; }

        public string Type { get; set; }

        public string DisplayNum { get; set; }

        public int OrderNum { get; set; }

        public string FullDisplayNum
        {
            get
            {
                var result = new StringBuilder(DisplayNum);
                switch (Type)
                {
                    case "1":
                        result.Append(" автобус");
                        break;
                    case "2":
                        result.Append(" троллейбус");
                        break;
                    case "3":
                        result.Append(" трамвай");
                        break;
                    case "8":
                        result.Append(" маршрутное такси");
                        break;
                }
                return result.ToString();
            }
        }
    }
}
