﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.ViewModels.Shared
{
    public class ScheduleViewModel
    {
        public string Id { get; set; }

        public string Title { get; set; }
    }
}
