﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Client.ViewModels.Shared
{
    public class StopScheduleViewModel
    {
        public string Name { get; set; }

        public List<DateTime> TimeList { get; set; }

        public List<HourScheduleViewModel> TimeListByHour { get; set; }

        public StopScheduleViewModel()
        {
            TimeList = new List<DateTime>();
            TimeListByHour = new List<HourScheduleViewModel>();
        }

        public void Calculate()
        {
            foreach (var hourGroup in TimeList.GroupBy(t => t.Hour))
            {
                var hourModel = new HourScheduleViewModel
                {
                    Hour = hourGroup.Key < 10 ? "0" + hourGroup.Key : hourGroup.Key.ToString()
                };
                foreach (var dtMinute in hourGroup)
                {
                    hourModel.Minutes.Add(dtMinute.ToString(":mm", CultureInfo.InvariantCulture));
                }
                TimeListByHour.Add(hourModel);
            }
        }
    }
}
