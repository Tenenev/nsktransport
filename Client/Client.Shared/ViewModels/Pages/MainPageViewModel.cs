﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Client.Common;
using Client.ViewModels.Shared;
using Microsoft.VisualBasic.CompilerServices;

namespace Client.ViewModels.Pages
{
    public class MainPageViewModel : ViewModelBase
    {
        private ResultHelper _resultHelper;

        public ObservableCollection<RouteViewModel> Routes { get; private set; }

        #region Search Route query

        private string _searchRouteQuery;

        public string SearchRouteQuery
        {
            get { return _searchRouteQuery; }
            set
            {
                if (SetField(ref _searchRouteQuery, value, "SearchRouteQuery"))
                {
                    if (SearchRoutesCommand.CanExecute(null))
                    {
                        SearchRoutesCommand.Execute(null);
                    }
                    else
                    {
                        _searchRoutesNextTime = true;
                    }
                }
            }
        }

        #endregion

        public ObservableCollection<RouteViewModel> FilteredRoutes { get; private set; }

        #region Selected Route

        private RouteViewModel _selectedRoute;

        public RouteViewModel SelectedRoute
        {
            get { return _selectedRoute; }
            set
            {
                if (SetField(ref _selectedRoute, value, "SelectedRoute"))
                {
                    if (SearchDirectionsCommand.CanExecute(null))
                    {
                        SearchDirectionsCommand.Execute(null);
                        scrollToHubSection(2);
                    }
                    else
                    {
                        _searchDirectionsNextTime = true;
                    }
                }
            }
        }

        #endregion

        public ObservableCollection<DirectionViewModel> Directions { get; private set; }

        #region Selected Direction

        private DirectionViewModel _selectedDirection;

        public DirectionViewModel SelectedDirection
        {
            get { return _selectedDirection; }
            set
            {
                if (SetField(ref _selectedDirection, value, "SelectedDirection"))
                {
                    if (SearchSchedulesCommand.CanExecute(null))
                    {
                        SearchSchedulesCommand.Execute(null);
                        scrollToHubSection(3);
                    }
                    else
                    {
                        _searchSchedulesNextTime = true;
                    }
                }
            }
        }

        #endregion

        public ObservableCollection<ScheduleViewModel> Schedules { get; private set; }

        #region Selected Schedule

        private ScheduleViewModel _selectedSchedule;

        public ScheduleViewModel SelectedSchedule
        {
            get { return _selectedSchedule; }
            set
            {
                if (SetField(ref _selectedSchedule, value, "SelectedSchedule"))
                {
                    if (SearchStopsCommand.CanExecute(null))
                    {
                        SearchStopsCommand.Execute(null);
                        scrollToHubSection(4);
                    }
                    else
                    {
                        _searchStopsNextTime = true;
                    }
                }
            }
        }

        #endregion

        public ObservableCollection<StopViewModel> Stops { get; private set; }

        #region Selected Stop

        private StopViewModel _selectedStop;

        public StopViewModel SelectedStop
        {
            get { return _selectedStop; }
            set
            {
                if (SetField(ref _selectedStop, value, "SelectedStop"))
                {
                    if (SearchResultsCommand.CanExecute(null))
                    {
                        SearchResultsCommand.Execute(null);
                        scrollToHubSection(5);
                    }
                    else
                    {
                        _searchResultsNextTime = true;
                    }
                }
            }
        }

        #endregion

        #region Result

        private ResultViewModel _result;

        public ResultViewModel Result
        {
            get { return _result; }
            set { SetField(ref _result, value, "Result"); }
        }

        #endregion

        public MainPageViewModel(ResultHelper resultHelper)
        {
            _resultHelper = resultHelper;
            Routes = new ObservableCollection<RouteViewModel>();
            FilteredRoutes = new ObservableCollection<RouteViewModel>();
            Directions = new ObservableCollection<DirectionViewModel>();
            Schedules = new ObservableCollection<ScheduleViewModel>();
            Stops = new ObservableCollection<StopViewModel>();
            Result = new ResultViewModel();
        }

        public event EventHandler<int> ShowHubSectionRequested;

        private void scrollToHubSection(int index)
        {
            if (ShowHubSectionRequested != null)
                ShowHubSectionRequested(this, index);
        }

        #region Commands

        #region Search Routes

        private RelayCommand _searchRoutesCommand;

        public ICommand SearchRoutesCommand
        {
            get { return _searchRoutesCommand ?? (_searchRoutesCommand = new RelayCommand(SearchRoutes, () => CanSearchRoutes)); }
        }

        private bool _canSearchRoutes = true;

        public bool CanSearchRoutes
        {
            get { return _canSearchRoutes; }
            set
            {
                _canSearchRoutes = value;
                if (_searchRoutesCommand != null)
                    _searchRoutesCommand.RaiseCanExecuteChanged();
            }
        }

        private bool _searchRoutesNextTime;

        public async void SearchRoutes()
        {
            CanSearchRoutes = false;
            var query = SearchRouteQuery;

            Result.Title = "";
            Result.Date = "";
            Result.Schedule.Clear();
            Stops.Clear();
            Schedules.Clear();
            Directions.Clear();
            FilteredRoutes.Clear();

            if (!string.IsNullOrWhiteSpace(query))
            {
                try
                {
                    var routes = await ApiHelper.GetRoutes(query);

                    foreach (var route in routes)
                    {
                        FilteredRoutes.Add(route);
                    }
                }
                catch
                {
                }
            }

            CanSearchRoutes = true;
            if (_searchRoutesNextTime)
            {
                _searchRoutesNextTime = false;
                SearchRoutes();
            }
        }

        #endregion

        #region Search Directions

        private RelayCommand _searchDirectionsCommand;

        public ICommand SearchDirectionsCommand
        {
            get { return _searchDirectionsCommand ?? (_searchDirectionsCommand = new RelayCommand(SearchDirections, () => CanSearchDirections)); }
        }

        private bool _canSearchDirections = true;

        public bool CanSearchDirections
        {
            get { return _canSearchDirections; }
            set
            {
                _canSearchDirections = value;
                if (_searchDirectionsCommand != null)
                    _searchDirectionsCommand.RaiseCanExecuteChanged();
            }
        }

        private bool _searchDirectionsNextTime;

        public async void SearchDirections()
        {
            CanSearchDirections = false;

            Result.Title = "";
            Result.Date = "";
            Result.Schedule.Clear();
            Stops.Clear();
            Schedules.Clear();
            Directions.Clear();

            if (SelectedRoute != null)
            {
                try
                {
                    var directions = await ApiHelper.GetDirections(SelectedRoute.Type, SelectedRoute.Id);

                    foreach (var direction in directions)
                    {
                        Directions.Add(direction);
                    }
                }
                catch
                {
                }
                
            }

            CanSearchDirections = true;
            if (_searchDirectionsNextTime)
            {
                _searchDirectionsNextTime = false;
                SearchDirections();
            }
        }

        #endregion

        #region Search Schedules

        private RelayCommand _searchSchedulesCommand;

        public ICommand SearchSchedulesCommand
        {
            get { return _searchSchedulesCommand ?? (_searchSchedulesCommand = new RelayCommand(SearchSchedules, () => CanSearchSchedules)); }
        }

        private bool _canSearchSchedules = true;

        public bool CanSearchSchedules
        {
            get { return _canSearchSchedules; }
            set
            {
                _canSearchSchedules = value;
                if (_searchSchedulesCommand != null)
                    _searchSchedulesCommand.RaiseCanExecuteChanged();
            }
        }

        private bool _searchSchedulesNextTime;

        public async void SearchSchedules()
        {
            CanSearchSchedules = false;

            Result.Title = "";
            Result.Date = "";
            Result.Schedule.Clear();
            Stops.Clear();
            Schedules.Clear();

            if (SelectedRoute != null && SelectedDirection != null)
            {
                try
                {
                    var schedules = await ApiHelper.GetSchedules(SelectedRoute.Type, SelectedRoute.Id, SelectedDirection.Id);

                    foreach (var schedule in schedules)
                    {
                        Schedules.Add(schedule);
                    }
                }
                catch
                {
                    
                }
            }

            CanSearchSchedules = true;
            if (_searchSchedulesNextTime)
            {
                _searchSchedulesNextTime = false;
                SearchSchedules();
            }
        }

        #endregion

        #region Search Stops

        private RelayCommand _searchStopsCommand;

        public ICommand SearchStopsCommand
        {
            get { return _searchStopsCommand ?? (_searchStopsCommand = new RelayCommand(SearchStops, () => CanSearchStops)); }
        }

        private bool _canSearchStops = true;

        public bool CanSearchStops
        {
            get { return _canSearchStops; }
            set
            {
                _canSearchStops = value;
                if (_searchStopsCommand != null)
                    _searchStopsCommand.RaiseCanExecuteChanged();
            }
        }

        private bool _searchStopsNextTime;

        public async void SearchStops()
        {
            CanSearchStops = false;

            Result.Title = "";
            Result.Date = "";
            Result.Schedule.Clear();
            Stops.Clear();

            if (SelectedRoute != null && SelectedDirection != null && SelectedSchedule != null)
            {
                try
                {
                    var stops = await ApiHelper.GetStops(SelectedRoute.Type, SelectedRoute.Id, SelectedDirection.Id, SelectedSchedule.Id);
                    Stops.Add(new StopViewModel { Id = "0", Title = "Все остановки" });
                    foreach (var stop in stops)
                    {
                        Stops.Add(stop);
                    }
                }
                catch
                {
                }
            }

            CanSearchStops = true;
            if (_searchStopsNextTime)
            {
                _searchStopsNextTime = false;
                SearchStops();
            }
        }

        #endregion

        #region Search Results

        private RelayCommand _searchResultsCommand;

        public ICommand SearchResultsCommand
        {
            get { return _searchResultsCommand ?? (_searchResultsCommand = new RelayCommand(SearchResults, () => CanSearchResults)); }
        }

        private bool _canSearchResults = true;

        public bool CanSearchResults
        {
            get { return _canSearchResults; }
            set
            {
                _canSearchResults = value;
                if (_searchResultsCommand != null)
                    _searchResultsCommand.RaiseCanExecuteChanged();
            }
        }

        private bool _searchResultsNextTime;

        public async void SearchResults()
        {
            CanSearchResults = false;

            Result.Title = "";
            Result.Date = "";
            Result.Schedule.Clear();

            if (SelectedRoute != null && SelectedDirection != null && SelectedSchedule != null && SelectedStop != null)
            {
                try
                {
                    var response = await ApiHelper.GetResult(SelectedRoute.Type, SelectedRoute.Id, SelectedDirection.Id, SelectedSchedule.Id, SelectedStop.Id);

                    var result = await _resultHelper.GetResult(response);

                    Result.Title = result.Title;
                    Result.Date = result.Date;

                    if (MaxMinutesCountChanged != null)
                    {
                        var maxMinutesCount = result.Schedule.Max(s => s.TimeListByHour.Max(t => t.Minutes.Count));
                        MaxMinutesCountChanged(this, maxMinutesCount);
                    }

                    foreach (var scheduleItem in result.Schedule)
                    {
                        Result.Schedule.Add(scheduleItem);
                    }
                }
                catch
                {
                }
            }

            CanSearchResults = true;
            if (_searchResultsNextTime)
            {
                _searchResultsNextTime = false;
                SearchResults();
            }
        }

        #endregion

        #endregion

        public event EventHandler<int> MaxMinutesCountChanged;
    }
}
