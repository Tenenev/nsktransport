﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Client.ViewModels.Shared;

namespace Client.Common
{
    public abstract class ResultHelper
    {
        public abstract Task<ResultViewModel> GetResult(string resultStr);
    }
}
