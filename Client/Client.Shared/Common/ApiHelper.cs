﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.Storage.Streams;
using Windows.Web.Http;
using Client.ViewModels.Shared;
using System.Runtime.InteropServices.WindowsRuntime;
using Client.Common;

namespace Client.Common
{
    public static class ApiHelper
    {
        public const string ApiBaseUrl = @"http://nskgortrans.ru/site/rasp?";
        public const string ResultBaseUrl = @"http://maps.nskgortrans.ru/components/com_planrasp/helpers/grasp.php?";

        private static async Task<string> GetResponse(string parameters, string baseUrl = ApiBaseUrl)
        {
            string response;

            using (var client = new HttpClient())
            {
                var stream = await client.GetInputStreamAsync(new Uri(baseUrl + parameters));

                byte[] bytes;

                using (var ms = new MemoryStream())
                {
                    stream.AsStreamForRead().CopyTo(ms);
                    bytes = ms.ToArray();
                }

                var utf8 = Encoding.UTF8;

#if WINDOWS_APP
                var win1251 = Encoding.GetEncoding("Windows-1251");
#endif
#if WINDOWS_PHONE_APP
                var win1251 = new Windows1251();
#endif

                var win1251Bytes = Encoding.Convert(utf8, win1251, bytes);

                response = win1251.GetString(win1251Bytes, 0, win1251Bytes.Length);
            }

            return response;
        }

        public static async Task<List<RouteViewModel>> GetRoutes(string searchStr)
        {
            var response = await GetResponse(string.Format("q={0}", searchStr));

            var list = new List<RouteViewModel>();

            var routeLineRegex = new Regex(@"^([^|]+)\|[^|]+\|(\d+)\|([^|]+)$");

            var notDigitRegex = new Regex(@"\D+");
            var removeBracketsRegex = new Regex(@"(?:\(\d+\))");

            foreach (var line in response.Split('\n').Where(line => !string.IsNullOrWhiteSpace(line)))
            {
                var match = routeLineRegex.Match(line);
                if(!match.Success) continue;

                var orderNumStr = notDigitRegex.Replace(removeBracketsRegex.Replace(match.Groups[1].Value, ""), "");

                list.Add(new RouteViewModel
                {
                    DisplayNum = match.Groups[1].Value,
                    Type = match.Groups[2].Value,
                    OrderNum = string.IsNullOrEmpty(orderNumStr) ? 0 : int.Parse(orderNumStr),
                    Id = match.Groups[3].Value
                });
            }

            return list.OrderBy(r => r.Type).ThenBy(r => r.OrderNum).ToList();
        }

        public static async Task<List<DirectionViewModel>> GetDirections(string type, string route)
        {
            var response = await GetResponse(string.Format("m={0}&t={1}", route, type));

            var doc = new XmlDocument();
            doc.LoadXml(response);

            var directionsNodes = doc.DocumentElement.SelectNodes("/routes/route");

            var directions = new List<DirectionViewModel>();
            foreach (XmlElement directionNode in directionsNodes)
            {
                directions.Add(new DirectionViewModel
                {
                    Id = directionNode.GetAttribute("id"),
                    Title = directionNode.GetAttribute("title")
                });
            }
            return directions;
        }

        public static async Task<List<ScheduleViewModel>> GetSchedules(string type, string route, string direction)
        {
            var response = await GetResponse(string.Format("m={0}&t={1}&z={2}", route, type, direction));

            var doc = new XmlDocument();
            doc.LoadXml(response);

            var scheduleNodes = doc.DocumentElement.SelectNodes("/schedules/schedule");

            var schedules = new List<ScheduleViewModel>();
            foreach (XmlElement scheduleNode in scheduleNodes)
            {
                schedules.Add(new ScheduleViewModel
                {
                    Id = scheduleNode.GetAttribute("id"),
                    Title = scheduleNode.GetAttribute("title")
                });
            }
            return schedules.OrderBy(s => s.Title).ToList();
        }

        public static async Task<List<StopViewModel>> GetStops(string type, string route, string direction, string schedule)
        {
            var response = await GetResponse(string.Format("m={0}&t={1}&z={2}&sch={3}", route, type, direction, schedule));

            var doc = new XmlDocument();
            doc.LoadXml(response);

            var stopsNodes = doc.DocumentElement.SelectNodes("/stops/stop");

            var stops = new List<StopViewModel>();
            foreach (XmlElement stopsNode in stopsNodes)
            {
                stops.Add(new StopViewModel
                {
                    Id = stopsNode.GetAttribute("id"),
                    Title = stopsNode.GetAttribute("title")
                });
            }
            return stops;
        }

        public static async Task<string> GetResult(string type, string route, string direction, string schedule,
            string stop)
        {
            return await GetResponse(string.Format("tv=mr&m={0}&t={1}&r={2}&sch={3}&s={4}&v=0", route, type, direction, schedule, stop), ResultBaseUrl);
        }
    }
}
